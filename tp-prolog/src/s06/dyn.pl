% =========================================================================
% FILE     : dyn.pl
% SUPPORT  : Bapst Frederic, HEIA-FR.
% CONTEXT  : Programmation Logique
% =========================================================================
% OVERVIEW : assert/retract in Prolog, and "univ" =..
% =========================================================================

%S6 EX3
%Ex3a) La règle 1 de fol/4 évaluera le paramètre A et mettera son résultat dans Res
fol([], _, A, Res) :- Res is A. % Res = A évalué
%Ex3a) La règle 2 de fol/4 accumule une liste d'opération défini par "Op"
%construite avec comme paramètre (dans l'ordre) les sous-opération et le première élément de la liste
%[E|Ls] fournie.
%Ex: fol([4,1,5,2],'+',0,Res) -> '+'('+'('+'('+'(0,4),1),5),2)-> 0 + 4 + 1 + 5 + 2 et Res = 12
fol([E|Ls], Op, Crt, Res) :-
    Crt1 =.. [Op, Crt, E], % Crt1 = Op(Crt,E)
    fol(Ls, Op, Crt1, Res).

%Ex3b) L'évaluation peut se faire à chaque opération et non à la fin.
fol2([], _, Res, Res).
fol2([E|Ls], Op, Crt, Res) :-
    fol2(Ls, Op, Crt, R),
    Crt1 =.. [Op, R, E],
    Res is Crt1.

% ------------------------------------------------------------
:- dynamic(hanoiDyn/5).
%EX 1
% --- hanoiDyn1(+NbOfDiscs, ?A, ?B, ?C, ?NbOfMoves).
% --- Avec assert/retract, nbre de mouvements

hanoiDyn(1,_A,_B,_,1.0).                      %--- move _A to _B
hanoiDyn(N,A,B,C,Moves) :-
            N>1,
            N1 is N-1,
            hanoiDyn(N1,A,C,B,Moves1),        %--- move N-1 discs
   asserta((hanoiDyn(N1,A,C,B,Moves1) :-! )),
                                              %--- move A to B
            hanoiDyn(N1,C,B,A,Moves2),        %--- move N-1 discs
   retract((hanoiDyn(N1,A,C,B,_     ) :-! )),
            Moves is Moves1 + Moves2 + 1.

% ------------------------------------------------------------
solve1(N, Moves) :- 
	[A, B, C] = [a, b, c],          % L2
	hanoiDyn(N, A, B, C, Moves).    % L1
solve2(N, Moves) :- 
	hanoiDyn(N, A, B, C, Moves),    % L1
	[A, B, C] = [a, b, c].          % L2
	
% ------------------------------------------------------------
% --- organize(...) : ????
%EX 2
%Ordonne les éléments de [X|Xs] en sauvegardant dans l'ordre croissant les éléments du tableau en tant que foncteur elt(X)
organize([X|Xs], Ys) :- 
        add(X, []), %voir add
        organize(Xs, Ys). % continue le programme
organize([], Ys) :-
        wrap(Ys). % voir wrap
% ----------------
%Si [X|Xs] connu : Nettoie le programme en supprimant tous les éléments de Xs (sous forme de elt(X)) du programme
%Si [X|Xs] inconnu :Récupère les foncteurs elt(X) se trouvant dans le programme dans l'ordre d'apparaitions
wrap([X|Xs]) :- %Sépare X de Xs
        elt(X),  %Vérifie l'existance de l'etl(X)
        convert(X, G), % G = elt(X)
        retract(G), %Supprime elt(X)  de la liste des foncteurs
        !,%Bloque la possibilité de récupérer un autre X après le premier trouvé
        wrap(Xs).%Continue l'execution
wrap([]).
% ----------------
%Ajouter dans le programme les foncteurs representant les éléments dans l'ordre croissant en récupérant
%tous les éléments précédement ajouter et en les réajoutant pour ajuster l'ordre
add(X, Gs):- %
        elt(Y), %Récupère le premier Y
        Y<X,  % Si Y < X
        convert(Y, G), % G = elt(Y)
        retract(G), %Supprime elt(Y)
        !, %bloque la possibilité de tester un autre Y en récursif après le premier Y<X
        add(X, [G|Gs]). % relance l'add de X en ayant elt(Y) ajouter dans Gs
add(X, Gs):- % Ajoute dans la mémoire
        convert(X, G), % G = elt(X)
        enrich([G|Gs]). % Sauvegarde tous les elt se trouvant dans G dans le programme

%Enregistre tous les éléments elt dans le programme
enrich([]).
enrich([G|Gs]) :- %Sépare G de GS -> G = elt(X)
        asserta(G), % sauvegarde elt(X)
        enrich(Gs). % continue
% ----------------
:- dynamic(elt/1).

% Transforme un élément X dans elt, utile pour assert/retract X
convert(X, elt(X)).
