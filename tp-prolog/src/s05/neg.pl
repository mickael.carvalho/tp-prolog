% =========================================================================
% FILE     : neg.pl
% SUPPORT  : Bapst Frederic, HEIA-FR.
% CONTEXT  : Techniques Avancées de Programmation 3 : Programmation Logique
% =========================================================================
% OVERVIEW : Negation in Prolog
% =========================================================================

% ------------------------------------------------------------
prime(2).
prime(3).
prime(5).

even(4).
even(2).

a(X)   :- prime(X).
a(8).
b(X)   :- prime(X), !.
b(8).
c(X)   :- !, prime(X).
c(8).
d(X)   :- prime(X), even(X), !.
d(8).
e(X)   :- even(X), !, prime(X).
e(8).
f(X)   :- even(7), !, X=7.
f(8).
g(X,Y) :- prime(X), even(Y), !.
g(8,8).
h(X,Y) :- prime(X), !, even(Y).
h(8,8).
i(X,Y) :- even(Y), !, prime(X).
i(8,8).



% ----------------------
owns1(Ys, X)     :- egal(Ys, [_|Ys1]), owns1(Ys1, X).
owns1([X|_], Y)  :- egal(Y,X).
owns2([X|_], Y)  :- egal(X,Y).
owns2(Ys, X)     :- owns2(Ys1, X),  egal(Ys,[_|Ys1]).	
owns3([X|_], X).
owns3([_|Ys], X) :- owns3(Ys, Z),  egal(X,Z).
owns4([X], X).
owns4([X,_], X).
owns4([_|Ys], X) :- owns4(Ys, X).
egal(A,A).	



% myTrue and myFail
myfail :- fail('a','b').
fail(A,A).

mytrue.


%TEST ex3


not(P) :- P, !, fail.
not(_).

p(5):- fail.
p(5).

t :- var(E),
 not(p(E)),
 nonvar(E).

%C'est impossible. Lorsque l'on parcours l'arbre. Si l'on réussi à faire le p,
%on obtiendra un fail du au not/1 et donc la valeur ne sera pas attribué
%et dans le cas ou le p est un fail, la valeur ne sera pas non plus attribué
%donc dans les deux cas, E sera une var et donc nonvar loupera.


% Ex 3b
% ------------------------------------------------------------
% ---    owns(+L, ?E) : E est un élément de la liste L

owns([X|_Xs], X).
owns([_Y|Ys], X) :- !,owns(Ys, X).

%Dans le cas d'un not(owns(_,a)).
%La boucle infini ne se créera pas et
%il répondra directement sans chercher plus loin
%il faut tenir compte qu'il inverse la réponse.

%Dans le cas ou les deux règles sont inversé.
%La boucle sera quand même présente car le not prendre la première règle.
%Il y aura rapidement une saturation de mémoire et donc un crash.

%Ex 3c
%Cut Rouge:
%owns([X|_Xs], X) :- !. <- Recherche OK, par contre en cas d'inconnu n'en donnera qu'une seule possibilité
%owns([_Y|Ys], X) :- owns(Ys, X),!. <- Recherche OK, par contre en cas d'inconnu ne donnera que deux possibilités

%Cut Vert:
%owns([_Y|Ys], X) :- ! owns(Ys, X).