% =========================================================================
% FILE     : s04lists.pl
% SUPPORT  : Bapst Frederic, HEIA-FR.
% CONTEXT  : Programmation Logique
% =========================================================================
% OVERVIEW : Lists in Prolog, accumulators
% =========================================================================

% ------------------------------------------------------------
%--- factorial(+N, ?F) : F is the factorial of N

factorial(0, 1).
factorial(N, F) :- 	
        N>0, 
        N1 is N-1, 
        factorial(N1, F1),
        F is F1 * N.

% TODO - A COMPLETER

% ------------------------------------------------------------
%--- listSum(+Ls, ?S) : Ls is a list of ints, S is the sum

% TODO - A COMPLETER


% ------------------------------------------------------------
% --- concat(?As, ?Bs, ?Cs, ?AsBsCs) : 
%        AsBsCs is the concatenation of As, Bs and Cs
% ---
concat1(As, Bs, Cs, Ls) :-
    append(As, Bs, Xs),
    append(Xs, Cs, Ls).
% ---
concat2(As, Bs, Cs, Ls) :-
    append(Xs, Cs, Ls),
    append(As, Bs, Xs).
% ---
concat3(As,Bs,Cs,Ls) :- var(Ls),
    concat1(As,Bs,Cs,Ls).
concat3(As,Bs,Cs,Ls) :- nonvar(Ls),
    concat2(As,Bs,Cs,Ls).

% ------------------------------------------------------------
% --- Ex. 4
% |?- countAB('t.txt').
%   le fichier t.txt contient 3 a et 25 b
% |?- countAB('noFile.zzz').
%   Oups, erreur I/O
% 
% Please verify the behavior of your code (there should be only one solution)

countAB(FileName):-
    catch(countABnoCatch(FileName),E,write('Ooops. Can\'t find file')).

countABnoCatch(FileName):-
    open(FileName, read, StreamID),
    countAB(StreamID,0,0,NAnew,NBnew),
    write('A: '), write(NAnew),
    write('B: '), write(NBnew), nl,
    close(StreamID).

countAB(StreamID,NA,NB,NAfinal,NBfinal):-
    get_char(StreamID,Char),
    checkChar(StreamID,Char,NA,NB,NAfinal,NBfinal).

checkChar(StreamID,L,NA,NB,NAfinal,NBfinal):-
    L \= 'a',
    L \= 'b',
    L \= end_of_file,
    countAB(StreamID,NA,NB,NAfinal,NBfinal).

checkChar(StreamID,'a',NA,NB,NAfinal,NBfinal):-
    increment(NA,NAnew),
    countAB(StreamID,NAnew,NB,NAfinal,NBfinal).

checkChar(StreamID,'b',NA,NB,NAfinal,NBfinal):-
    increment(NB,NBnew),
    countAB(StreamID,NA,NBnew,NAfinal,NBfinal).

checkChar(StreamID,end_of_file,NA,NB,NA,NB).

increment(N,Nnew):-
    Nnew is N+1.

% ------------------------------------------------------------
% --- p(+Ls, ?Rs): ...

% exa
%Construit toutes les permutations possibles
%Verifie que les listes sont simplement deux permutations différentes
% exb
p(Ls, [E|Rs]) :-
    append(As, [E|Bs], Ls),
    append(As, Bs, Xs),
    p(Xs,Rs).
p([], []).
%ex c
r(Ls,Rs):-
    nonvar(Ls),
    p(Ls,Rs).
r(Ls,Rs):-
    nonvar(Rs),
    p(Rs,Ls).
%ex d
q(Ls, [E|Rs]) :-
    select(E, Ls, Bs),
    q(Bs,Rs).
q([], []).

