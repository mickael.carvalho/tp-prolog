% =========================================================================
% FILE     : bb_sorting.pl
% SUPPORT  : Bapst Frederic, HEIA-FR.
% CONTEXT  : Techniques Avancées de Programmation 3 : Programmation Logique
% =========================================================================
% OVERVIEW : Blackboard program for sorting
% =========================================================================

:- include('bb.pl').

%---------------------------------------------------------
%--- INPUT : set of numbers, and the term n(0)
%--- OUTPUT: set of terms e(Index,Value) giving the ascending order
%---------------------------------------------------------
%input/condition ~~> action/output
    [A,n(X)]/[\+ unify_with_occurs_check(A,e(_,_))] ~~> [Y is X+1]/[e(X,A),n(Y)] .
    [e(I1,A1),e(I2,A2)]/[I1>I2,A1<A2] ~~> []/[e(I1,A2),e(I2,A1)].

test_bb_sorting :- BB=[ 8,2,5,9, n(0) ], 
                   bb_run(BB,BB1),nl,write(BB1). 

% should be : [e(0,2),e(3,9),n(4),e(1,5),e(2,8)]
