% =========================================================================
% FILE     : bb.pl
% SUPPORT  : Bapst Frederic, HEIA-FR.
% CONTEXT  : Techniques Avancées de Programmation 3 : Programmation Logique
% =========================================================================
% OVERVIEW : Blackboard engine
% =========================================================================

:-  op(650, xfx, '~~>').
%---------------------------------------------------------
%--- bb_run(+InitialBlackboard, -FinalBlackboard)
%---------------------------------------------------------
bb_run(BB, BBFinal) :-
    bb_filter(BB, Rules),
    bb_applySome(BB, Rules, BBFinal).


%--- bb_applySome(+BB, +PossibleRules, -BBFinal) :
%      BBFinal is the result when applying a rule randomly taken
%      from the list, then running the rest of the program

bb_applySome(BB, [], BB). % no more possible rules, exit.
bb_applySome(BB,Rules,BBFinal) :-
    length(Rules, L),
    random(0, L, N),
    nth0(N, Rules, Rule),
    bb_applyRule(BB, Rule, BB2),
    !,
    bb_run(BB2, BBFinal).

%--- bb_oneRule(+BB, -R) : R is a rule with IN data in blackboard,
%                          and valid Conditions
bb_oneRule(BB, In/Cond~~>_Body) :-
    In/Cond~~>_Body,
    bb_extract(BB, In, _BB2),
    bb_solve(Cond).

%--- bb_filter(+BB, -Rs) : Rs contains every matching rule with respect to BB
bb_filter(BB, Rs) :-
    findall(R, bb_oneRule(BB,R), Rs).

%--- bb_solve(+GoalList) : solves each goal in the list
bb_solve([]).
bb_solve([Goal | GoalList]) :-
    Goal,
    bb_solve(GoalList).

%--- bb_applyRule(+BB, +Rule, -NewBB) : NewBB is the blackboard after
%                                      removing IN data from bb,
%                                      playing actions, and
%                                      adding OUT data into BB
bb_applyRule(BB, In/_Cond~~>Action/Out, NewBB1) :-
    bb_extract(BB, In, BB2),
    bb_solve(Action),
    append(BB2, Out, NewBB1).

%--- bb_extract(+BB, +InList, -NewBB) : NewBB is BB without In elements
bb_extract(BB, [], BB) :- !.
bb_extract(BB, [I | Is], NewBB) :-
    select(I, BB, BB1),
    bb_extract(BB1, Is, NewBB).


%---bb_db(+BB) : current blackboard tracing
bb_db(BB) :- write(BB), nl.
% bb_db(BB) :- write(BB), nl, get_char(_).

