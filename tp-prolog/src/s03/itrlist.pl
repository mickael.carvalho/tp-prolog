% =========================================================================
% FILE     : itrlist.pl
% SUPPORT  : Bapst Frederic, HEIA-FR.
% CONTEXT  : Techniques Avancées de Programmation 3 : Programmation Logique
% =========================================================================
% OVERVIEW : Abstract Data Types in Prolog - Iterative lists
% =========================================================================

% ------------------------------------------------------------
% --- Test programs
% ------------------------------------------------------------
adt_test_apply(_, Adt, [], Adt).
adt_test_apply(G, Adt, [C|Cmds], AdtEnd) :-
        Goal =.. [G, Adt, C, Adt1],
        write(trying(Goal)), nl,
        Goal,
        write(done(Goal)), nl,
        adt_test_apply(G, Adt1, Cmds, AdtEnd).
% ------------------------------------------------------------
itrList_test :-
        itrList_new(ItrList),
        Cmds = [insertAfter(a),
                insertAfter(b),
                goToNext,
                consultAfter(a),
                removeAfter(a),
                isLast,
                insertAfter(d),
                goToPrev,
                consultAfter(B)
               ],
        adt_test_apply(itrList_apply, ItrList, Cmds, ItrList1),
     \+ itrList_apply(ItrList1, isLast,_),
        assertEquals(b, B),
        write('Test passed successfully.').

assertEquals(Expected, Effective) :-
	Expected==Effective.
assertEquals(Expected, Effective) :-
	Expected\==Effective,
	write('bad news... Expected: '), write(Expected),
	write(' Effective: '), write(Effective), nl, 
	fail.
% ------------------------------------------------------------
% itrList : Iterative List ADT, with current arc
%    Operations :
%      new(-X),  goToNext,  insertAfter(+X),
%      isFirst,  goToPrev, consultAfter(?X), 
%      isLast,              removeAfter(?Removed)
%    Representation : 
%      e(ListTowardsFirst, ListTowardsEnd)

% itrList_new(...).

% itrList_apply(..., goToNext, ...).
% itrList_apply(..., goToPrev, ...).
% ...

%TODO
itrList_new(e([],[])).

itrList_apply(e(Forward,[A|Backward]),goToNext,e([A|Forward],Backward)).
itrList_apply(e(Forward,Backward),insertAfter(A),e(Forward,[A|Backward])).
itrList_apply(e(Forward,[A|Backward]),consultAfter(A),e(Forward,[A|Backward])).
itrList_apply(e(Forward,[A|Backward]),removeAfter(A),e(Forward,Backward)).
itrList_apply(e([],Backward),isFirst,e([],Backward)).
itrList_apply(e(Forward,[]),isLast,e(Forward,[])).
itrList_apply(e([A|Forward],Backward),goToPrev,e(Forward,[A|Backward])).