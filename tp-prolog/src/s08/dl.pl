% =========================================================================
% FILE     : dl.pl
% SUPPORT  : Bapst Frederic, HEIA-FR.
% CONTEXT  : Techniques Avancées de Programmation 3 : Programmation Logique
% =========================================================================
% OVERVIEW : D-Lists in Prolog
% =========================================================================

% ------------------------------------------------------------
% --- concatDL(+Xs, +Ys, ?Zs) : Zs is the concatenation of
%                               Xs and Ys (repr. as D-lists)
% --- concatDL(Xs-DXs, Ys-DYs, Zs-DZs) :- DXs=Ys, Zs=Xs, DZs=DYs.
concatDL(Xs-Ys, Ys-DYs, Xs-DYs).

% ------------------------------------------------------------
% myList: a list where you can remove from one extremity, and add at both
%    OPERATIONS: new(-X), consultFirst(?Elt), removeFirst(?Elt) 
%                         addFirst(+Elt), addLast(+Elt)
%              maybe: consultLast(?Elt)
%    REPRESENTATION: based on a D-List
%    COMPLEXITY:     CPU O(1), RAM O(n) 

% --- A COMPLETER

myList_new(X-X).

myList_apply(X-Dx,      addLast(A), R) :-
    concatDL(X-Dx,[A|L]-L,R).
myList_apply(X-Dx,     addFirst(A), R) :-
    concatDL([A|L]-L,X-Dx,R).
myList_apply([A|Xs]-Dx,  removeFirst(A), Xs-Dx):-
    [A|Xs] \== Dx.
myList_apply([A|Xs]-Dx, consultFirst(A), [A|Xs]-Dx):-
    [A|Xs] \== Dx.

% --------------------------
adt_test_apply(_, Adt, [], Adt).
adt_test_apply(G, Adt, [C|Cmds], AdtEnd) :-
        Goal =.. [G, Adt, C, Adt1],
        write(trying(Goal)), nl,
        Goal,
        write(done(Goal)), nl,
        adt_test_apply(G, Adt1, Cmds, AdtEnd).   
% --------------------------
myList_test :-
        myList_new(Fifo),
        Cmds = [addLast(a), 
                addLast(b), 
                addLast(c),
                consultFirst(X1), 
                addFirst(d),
                consultFirst(X2),
                removeFirst(X3), 
                consultFirst(X4),
                removeFirst(X5), 
                removeFirst(X6), 
                addLast(e),
                removeFirst(X7), 
                removeFirst(X8)
                ],
        [X1,X2,X3,X4,X5,X6,X7,X8]=[a,d,d,a,a,b,c,e],
        adt_test_apply(myList_apply, Fifo, Cmds, Fifo1),
     \+ myList_apply(Fifo1, consultFirst(_),_),
     \+ myList_apply(Fifo1, removeFirst(_),_).


% ------------------------------------------------------------
% --- inverseDL1(+XDs, ?YDs) : YDs is the D-List inverse of D-List XDs
inverse([],[]).
inverse([X|Xs],Ys):-
    inverse(Xs,Zs),
    append(Zs,X,Ys).



%fakeInverseDL1(X-X,X-X).
% X <=> [X]




inverseDL1(X-Ls,X-Ls):-
    X == Ls.

inverseDL1([X|Xs]-Ls,Z):-
    inverseDL1(Xs-Ls,Y),
    concatDL(Y,[X|L]-L,Z).

% ------------------------------------------------------------
f_test(Result) :- 
    X = [ [a,b|R], [c,d,e,f|S], [f,g|T] ] - [R,[f|S],T,J],
    Y = [ [h,i|U], [j,k|V], [l,m,n|W] ] - [U,V,W],
    concatDL(X, Y, Result).

%--- g(+CDL, ?Result)
g(([X|As]-[Y])-(Bs-_), X-Y) :- concatDL(_-As, Bs-_, X-Y). 

g_test(Result) :- 
    La  = [ [a,b|A], [c,d,e|B], [f,g|C] | Lad ] - Lad,
    DLa = [A,B,C | DLad] - DLad,
    CDLa = La - DLa,
    g(CDLa, Result).

